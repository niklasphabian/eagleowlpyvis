import sqlite3
import datetime
import pandas
import matplotlib.pyplot as plt
import numpy
import matplotlib.ticker as mtick


class Database:
    def __init__(self):
        self.openConnection()

    def openConnection(self):
        self.conn = sqlite3.connect('eagleowl.db')
        self.cursor = self.conn.cursor()

    def closeConnection(self):
        self.conn.close()

    def getCompleteTS(self):
        self.cursor.execute('SELECT * FROM energy_history')
        ret = self.cursor.fetchall()
        return ret

    def getHours(self):
        hoursMean = []
        for hour in range(24) :
            self.cursor.execute('SELECT ch1_kw_avg FROM energy_history WHERE hour = {}'.format(hour))
            ret = self.cursor.fetchall()
            ret = self.convertArray(ret)
            hoursMean.append(numpy.mean(ret)*60/1000)   # convert Wh to kW
        return hoursMean

    def getHoursWeekend(self):
        hoursMean = []
        for hour in range(24) :
            self.cursor.execute('SELECT ch1_kw_avg FROM energy_history WHERE hour = {}'.format(hour))
            ret = self.cursor.fetchall()
            ret = self.convertArray(ret)
            hoursMean.append(numpy.mean(ret)*60/1000)   # convert Wh to kW

    def convertArray(self, inArray):
        outArray = []
        for row in inArray:
            outArray.append(row[0])
        return outArray
    
    def clean_database(self):
        query = "DELETE FROM energy_history WHERE YEAR > 2100 OR YEAR < 2014"
        self.cursor.execute(query)
        self.conn.commit()
        
    
class eagleData:
    def __init__(self):
        self.myDB = Database()
        self.myDB.clean_database()
        figure, self.axes = plt.subplots(nrows=2, ncols=2)

    def setupAxes(self):
        pass

    def loadTimeSeries(self):
        data = self.myDB.getCompleteTS()
        self.parseData(data)

    def parseData(self, data):
        dates = []
        power = []
        for row in data:
            dates.append(self.convertDate(row))
            power.append(row[7])
        self.dataframe = pandas.Series(data=power, index=dates)

    def convertDate(self, row):
        year = row[1]
        month = row[2]        
        day = row[3]
        hour = row[4]
        minute = row[5]        
        return datetime.datetime(year, month, day, hour, minute)

    def plotTimeSeries(self):
        self.loadTimeSeries()
        resampled = self.dataframe.resample("1min") * 60/1000 # Convert Wh/min into kW
        resampled = self.dataframe * 60 / 1000        
        resampled.plot(ax=self.axes[1][1])
        self.axes[1][1].set_xlabel('Time')
        self.axes[1][1].set_ylabel('Power in kW')
        self.axes[1][1].set_ylim(bottom= 0)
        
    def plotHours(self):
        hours = self.myDB.getHours()
        self.axes[0][0].plot(hours)
        self.axes[0][0].set_xlabel('Time')
        self.axes[0][0].set_ylabel('Power in kW')
        self.axes[0][0].set_xlim(0, 24)
        self.axes[0][0].grid()

    def plotWeekday(self):
        days = []
        totEnergy = sum(self.dataframe)
        for day in range(7) :
            nDays = len(self.dataframe[self.dataframe.index.weekday == day])/24/60
            days.append(sum(self.dataframe[self.dataframe.index.weekday == day])/nDays/1000)
        average = self.dataframe.mean()*24*60/1000
        self.axes[0][1].plot([0,8],[average, average])
        x = [1,2,3,4,5,6,7]
        my_xticks = ['Monday','Tuesday','Wednesday','Thursday', 'Friday', 'Saturday', 'Sunday']
        self.axes[0][1].set_xticks(x, minor=False)
        self.axes[0][1].set_xticklabels(my_xticks, fontdict=None, minor=False)
        self.axes[0][1].bar(x, days, align='center')
        self.axes[0][1].set_ylabel('Average energy in kWh')
        self.axes[0][1].set_xlim(0, 8)
        self.axes[0][1].grid()

    def plotAvDemandDev(self):
        enTot = 0
        en = []
        idx = 1
        for row in self.dataframe:
            enTot = enTot + row*60*24/1000
            en.append(enTot/idx)
            idx = idx + 1
        enSeries = pandas.Series(data=en, index=self.dataframe.index)
        enSeries.plot(ax=self.axes[1][0])
        self.axes[1][0].set_ylabel('Average daily demand in kWh')
        self.axes[1][0].set_ylim(0, 4)
        
    def plotAvgWeek(self):
        resampled = self.dataframe.resample("W", how='mean') * 60/1000*24 # Convert Wh/min into kW
        resampled.plot(ax=self.axes[1][0])
        self.axes[1][0].set_xlabel('Time')
        self.axes[1][0].set_ylabel('Average daily demand in kWh')
        self.axes[1][0].set_ylim(bottom= 0)

    def plotAvgMonth(self):
        resampled = self.dataframe.resample("M", how='mean') * 60/1000*24 # Convert Wh/min into kW
        resampled.plot(ax=self.axes[1][0])
        self.axes[1][0].set_xlabel('Time')
        self.axes[1][0].set_ylabel('Average daily demand in kWh')
        self.axes[1][0].set_ylim(bottom= 0)

    def show(self):
        plt.show()




def main():
    myData = eagleData()
    myData.loadTimeSeries()
    myData.plotTimeSeries()
    myData.plotHours()
    myData.plotWeekday()
    #myData.plotAvDemandDev()
    #myData.plotAvgWeek()
    myData.plotAvgMonth()
    myData.show()



main()

