import usb 
import time


class OWL():
    def __init__(self):
        self.connect_usb()
        self.detach_kernel()
        self.get_configuration()
        self.get_interface()
        self.get_endpoint_out()
        self.get_endpoint_in()
            
    def connect_usb(self):
        self.device = usb.core.find(idVendor=0x0fde, idProduct=0xca05)
        if self.device is None:
            raise ValueError('OWL is not connected')
        
    def detach_kernel(self):
        try:
            self.device.detach_kernel_driver(0)
        except:
            pass
        
    def get_configuration(self):
        self.device.set_configuration(1)
        self.configuration = self.device.get_active_configuration()
        
    def get_interface(self):
        self.interface = self.configuration[(0,0)]
        
    def get_endpoint_out(self):
        self.endpoint_out = usb.util.find_descriptor(self.interface, custom_match = lambda e: usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_OUT)
        print(self.endpoint_out)
        
    def get_endpoint_in(self):
        self.endpoint_in = usb.util.find_descriptor(self.interface, custom_match = lambda e: usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_IN)
        print(self.endpoint_in)
        
    def release(self):
        usb.util.release_interface(self.device, self.interface)
        usb.util.dispose_resources(self.device)
    


owl = OWL()
msg = '5A'
#msg = 'test'
for val in range(80,80+512):
    addr = hex(val)
    ret = owl.device.write(0x1, addr, 1000) 
    ret = owl.device.read(0x82, 64, 1000)
    print(ret)
    time.sleep(4)
    
owl.release()
    
    





